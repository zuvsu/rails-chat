class MessagesController < ApplicationController

  def index
    if cookies[:user]
      render 'messages/index'
    else
      redirect_to "/login"
    end
  end

  def list
    if cookies[:user]
      @messages = Message.all
      render text: @messages.to_json  # формат передачи данных Json
    else
      # если пользователь не произвел ранее логин (отсутсвует куки), отобразить not found страницу
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def create
    if cookies[:user]
      @message = Message.new(:content => params[:msg], :name => cookies[:user], :created_at => DateTime.now)
      @message.save
      render text: "OK"
    else
      # если пользователь не произвел ранее логин (отсутсвует куки), отобразить not found страницу
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def login
    # если http get запрос, то отображаем логин страницу
    # если иначе пользователь пытается сделать логин
    if request.get?
      render "messages/login"
    else
      cookies[:user] = {
          value: params[:login],
          expires: 1.year.from_now
      }
      redirect_to "/messages"
    end
  end
end
