//This is a manifest file that'll be compiled into application.js, which will include all the files
//listed below.
//
//Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
//or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
//It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
//the compiled file.
//
//WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
//GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

// updateDelay - интервал через которое обновляется чат
var UpdateDelay = 10000;
var EnterKeyCode = 13;

$(document).ready(function() {
    updateChat();   // запросить данные с сервера
    $("#errorMsg").fadeOut(1);

    $('#submit').click(function(event) {
        event.preventDefault();
        addNewText();
    });


    $("textarea").keypress(function(event) {
        // проверяем нажатие enter без шифта для добавления сообщения в чат
        if (event.keyCode == EnterKeyCode && !event.shiftKey) {
            event.preventDefault();
            addNewText();
        }
    });

    $("textarea").keypress(function(event){
        // проверяем нажатие enter + shift для перехода на следующую строку
        if (event.keyCode == EnterKeyCode && event.shiftKey)
        {
            event.preventDefault();
            $('#newMessage').val( $('#newMessage').val() + "\n");
        }
    });


});

// функция добавляет текст и проверяет на ошибки. Посылает данные с помощью Ajax
function addNewText() {

    var txt = $('#newMessage').val();
    if (isBlank(txt)) {
        $('#errorMsg').removeClass();
        $('#errorMsg').fadeIn(1).addClass("msgRed").text('Ваше сообщение пустое!!!').fadeOut(4000);
        clearMessageText();
        return false;
    }

    txt = trim(txt);
    if (txt.length < 2) {
        $('#errorMsg').removeClass();
        $('#errorMsg').fadeIn(1).addClass("msgRed").text('Сообщение должно содержать больше 1 символа').fadeOut(4000);
        clearMessageText();
        return false;
    }

    var dataMap = {msg: txt};
    $.ajax({type: "POST", data: dataMap ,url: "post", success:function() {
        clearMessageText();
        updateChat();
        $('#errorMsg').removeClass();
        $('#errorMsg').fadeIn(1).addClass("msgGreen").text('Ваше сообщение отправленно').fadeOut(4000);
    }, error:function(){
        $('#errorMsg').removeClass();
        $('#errorMsg').fadeIn(1).addClass("msgRed").text('Ошибка отправки данных').fadeOut(4000);
    }
    });

    clearMessageText();
}

// функция очищает поле ввода текста
function clearMessageText() {
    $('#newMessage').val('');
}

// функция проверяет присутствие в строке пробелов, табуляторов
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

// функция убирает слева и справа пробелы и табуляторы
function trim(str) {
    return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

// функция обновляет чат с помощью Ajax. Формат передачи данных Json
function updateChat() {

    $.ajax({url: "list", success:function(result) {
        $("#list").empty();
        var obj = jQuery.parseJSON(result);
        for(var i = 0; i < obj.length; i++) {
            var classNum = i % 3;
            var className;
            if (classNum == 0)
                className = "red"
            else if (classNum == 1)
                className = "green"
            else
                className = "yellow"


            var msg = obj[i];
            var div = $(document.createElement('div')).addClass(className);
            var spanDate = $(document.createElement('span')).addClass("right");
            var spanName = $(document.createElement('span')).addClass("nameMsg");
            var spanContent = $(document.createElement('span')).addClass("contentMsg");
            var msgDate = new Date(msg.created_at);
            var day = msgDate.getDate();
            var month = monthName(msgDate.getMonth());
            var year = msgDate.getFullYear();
            var hours = msgDate.getHours();
            var minutes = msgDate.getMinutes();
            var formattedMsgDate = day + " " + month + " " + year + ", " + hours + ":" + minutes;

            spanDate.html(formattedMsgDate);
            spanName.html('Сообщение от: ' + msg.name);
            spanContent.html(msg.content);
            div.append(spanName);
            div.append(spanContent);
            div.append(spanDate);
            $("#list").append(div);
        }
        $('#list').scrollTop($('#list')[0].scrollHeight);
    }, error:function(){
        $('#errorMsg').removeClass();
        $('#ErrorMsg').fadeIn(1).addClass("msgRed").text('Ошибка получения данных').fadeOut(2000);
    }});
    $('#ErrorMsg').text('');
    setTimeout(updateChat, UpdateDelay);
};

function monthName(num){
    ++num
    if (num == 01)
        return "января"
    else if (num == 02)
        return "февраля"
    else if (num == 03)
        return "марта"
    else if (num == 04)
        return "апреля"
    else if (num == 05)
        return "мая"
    else if (num == 06)
        return "июня"
    else if (num == 07)
        return "июля"
    else if (num == 08)
        return "августа"
    else if (num == 09)
        return "сентября"
    else if (num == 10)
        return "октября"
    else if (num == 11)
        return "ноября"
    else if (num == 12)
        return "декабря"
    else
        return num
}


